const TableModel = {
  AttributeDefinitions: [],
  KeySchema: [],
  TableName: '',
  ProvisionedThroughput: {}
};

const PetModel = {
  owner: 'OWNER',
  name: 'NAME'
};

module.exports = {
  TableModel, PetModel
};