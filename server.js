'use strict';

const express = require('express');

const PORT = 8081;
const HOST = '0.0.0.0';

const app = express();

require('./routes/index')(app);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

//Error Handler
app.use(function (err, req, res) {
  console.error(err.stack);
  res.status(500).send('Error is: ', err);
});



