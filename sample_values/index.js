const {PetModel} = require('../models/table');

const dbData = {
  TableName: 'Pets',
  KeySchema: [
    {AttributeName: PetModel.owner, KeyType: 'HASH'},
    {AttributeName: PetModel.name, KeyType: 'RANGE'}
  ],
  AttributeDefinitions: [
    {AttributeName: PetModel.owner, AttributeType: 'S'},
    {AttributeName: PetModel.name, AttributeType: 'S'}
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 10,
    WriteCapacityUnits: 10
  }
};

const deleteDBData = {
  TableName: 'Pets'
};

const getRequestData = {
  TableName: 'Pets',
  Key: {
    [PetModel.name]: 'Petya',
    [PetModel.owner]: 'Trump'
  }
};

const postRequestData = {
  TableName: 'Pets',
  Item: {
    [PetModel.owner]: 'Trump',
    [PetModel.name]: 'Petya'

  },
  ReturnConsumedCapacity: "TOTAL",
};

const updateRequestData = {
  TableName: 'Pets',
  Key: {
    [PetModel.owner]: 'Trump',
    [PetModel.name]: 'Petya'
  },
  ExpressionAttributeValues: {
    ':a': 11,
    ':add': {
      MEAL: 'meat',
      LEISURE: 'sleeping'
    }
  },
  UpdateExpression: 'set AdditionalInfo = :add, AGE = :a',
  ReturnValues: 'UPDATED_NEW'
};

const deleteRequestData = {
  TableName: 'Pets',
  Key: {
    [PetModel.owner]: 'Trump',
    [PetModel.name]: 'Vasya'
  },
  ReturnValues: 'ALL_OLD'
};

module.exports = {
  dbData, getRequestData, postRequestData, updateRequestData, deleteRequestData, deleteDBData
};