'use strict';

const tableService = require('../services/tables');

const createTable = async (data) => {
  try {
    return await tableService.createTable(data);
  } catch (err) {
    throw new Error(err);
  }
};

const deleteTable = async (deleteDBData) => {
  try {
    return await tableService.deleteTable(deleteDBData);
  } catch (err) {
    throw new Error(err);
  }
};

const insertItem = async (petData) => {
  try {
    return await tableService.insertItem(petData);
  } catch (err) {
    throw new Error(err);
  }
};

const readItem = async (petKey) => {
  try {
    return await tableService.readItem(petKey);
  } catch (err) {
    throw new Error(err);
  }
};

const updateItem = async (petData) => {
  try {
    return await tableService.updateItem(petData);
  } catch (err) {
    throw new Error(err);
  }
};

const deleteItem = async (petKey) => {
  try {
    return await tableService.deleteItem(petKey);
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  createTable, insertItem, readItem, updateItem, deleteItem, deleteTable
};