'use strict';

const {db, dbClient} = require('../config/database');

const createTable = async (dbData) =>
  await db.createTable(dbData).promise();

const deleteTable = async (deleteDBData) =>
  await db.deleteTable(deleteDBData).promise();

const insertItem = async (postRequestData) =>
  await dbClient.put(postRequestData).promise();

const readItem = async (getRequestData) =>
  await dbClient.get(getRequestData).promise();

const updateItem = async (putRequestData) =>
  await dbClient.update(putRequestData).promise();

const deleteItem = async (deleteRequestData) =>
  await dbClient.delete(deleteRequestData).promise();


module.exports = {
  createTable, insertItem, readItem, updateItem, deleteItem, deleteTable
};