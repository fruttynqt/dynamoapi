const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-2'});

const db = new AWS.DynamoDB({apiVersion: '2012-10-08'});
const dbClient = new AWS.DynamoDB.DocumentClient();

module.exports = {
  db, dbClient
};
