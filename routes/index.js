const tableController = require('../controllers');
const {dbData, getRequestData, postRequestData, updateRequestData, deleteRequestData, deleteDBData} = require('../sample_values/index');
const {errorHandle} = require('../helpers');

module.exports = function (app) {

  app.get('/', (req, res) => {
    res.send('Hello world\n');
  });

  app.post('/database', errorHandle(async (req, res) => {
    const createdDB = await tableController.createTable(dbData);
    console.log(createdDB);
    return res.send(createdDB);
  }));

  app.delete('/database', errorHandle(async (req, res) => {
    const deletedDB = await tableController.deleteTable(deleteDBData);
    return res.send(deletedDB);
  }));

  app.get('/pet', errorHandle(async (req, res) => {
    const result = await tableController.readItem(getRequestData);
    return res.send(result);
  }));
  app.post('/pet', errorHandle(async (req, res) => {
    const petInfo = await tableController.insertItem(postRequestData);
    return res.send(petInfo);
  }));

  app.put('/pet', errorHandle(async (req, res) => {
    const updatedPet = await tableController.updateItem(updateRequestData);
    return res.send(updatedPet);
  }));

  app.delete('/pet', errorHandle(async (req, res) => {
    const deletedPet = await tableController.deleteItem(deleteRequestData);
    return res.send(deletedPet);
  }))
};